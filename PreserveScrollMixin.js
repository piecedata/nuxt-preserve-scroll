const options = JSON.parse(`<%= JSON.stringify(options) %>`);
// extract the namespace from the options
const { namespace } = options;

let closeTime = Number(localStorage.getItem(`${namespace}CloseTime`));
closeTime = !Number.isNaN(closeTime) ? closeTime : 0;

let closePos = Number(localStorage.getItem(`${namespace}ClosePos`));
closePos = !Number.isNaN(closePos) ? closePos : 0;

window.addEventListener('beforeunload', () => {
  localStorage.setItem(`${namespace}CloseTime`, String(new Date().getTime()));
  localStorage.setItem(`${namespace}ClosePos`, String(window.scrollY));
});

export default {
  mounted() {
    // if we have hash try to find the element and scroll to id
    const elId = this.$route.hash ? this.$route.hash.slice(1) : '';

    if (elId) {
      const el = document.getElementById(elId);

      if (el && el.scrollIntoView) {
        el.scrollIntoView(true);
        return;
      }
    }

    if (new Date().getTime() - closeTime < options.timeout) {
      window.scrollTo(0, closePos);
    }
  },
};
