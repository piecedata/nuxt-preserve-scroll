import { resolve, join } from 'path';

export default function (moduleOptions) {
  const options = {
    ...{
      timeout: 800
    },
    ...moduleOptions,
    ...this.options.preserveScroll,
  };

  if (!options.namespace) {
    options.namespace = 'preserveScroll';
  }

  const { namespace } = options;

  this.addPlugin({
    src: resolve(__dirname, 'plugin.js'),
    fileName: join(namespace, 'plugin.js'),
    ssr: false,
    options,
  });

  this.addTemplate({
    src: resolve(__dirname, 'PreserveScrollMixin.js'),
    fileName: join(namespace, 'PreserveScrollMixin.js'),
    ssr: false,
    options,
  });
}

module.exports.meta = require('./package.json');
