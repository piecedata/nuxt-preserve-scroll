import PreserveScrollMixin from './PreserveScrollMixin';

function extend(app, mixin) {
  if (!app.mixins) {
    app.mixins = [];
  }
  app.mixins.push(mixin);
}

export default function ({ app }) {
  extend(app, PreserveScrollMixin);
}
